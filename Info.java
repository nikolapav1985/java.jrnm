/**
*
* Info class
*
* example of generics
*
*/
class Info<T>{
    protected T item;
    public void add(T item){ // set item
        this.item=item;
    }
    public T get(){ // get item
        return this.item;
    }
}
