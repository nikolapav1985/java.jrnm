/**
*
* Main class
*
* run example of generics
*
* ----- compile -----
*
* javac Main.java
*
* ----- run -----
*
* java Main
*
*/
class Main{
    public static void main(String args[]){
        Info<String> a=new Info<String>();
        Info<Integer> b=new Info<Integer>();
        a.add(new String("Hello"));
        b.add(new Integer(11));
        System.out.println("String content "+a.get());
        System.out.println("Integer content "+b.get());
    }
}
